import Database from "../../../src/core/database/Database";

describe("Database", () => {
  describe("connect", () => {
    it("Connects to the database", async () => {
      let error;
      try {
        await Database.connect();
        Database.disconnect();
      }catch(e) {
        error = e;
      }

      expect(error).toBeUndefined();
    })
  })
})
