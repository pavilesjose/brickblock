import dotenv from "dotenv";
dotenv.config();
import PgString from "pg-connection-string";

const herokuConnection = PgString.parse(process.env.DATABASE_URL);
module.exports = {
  username: herokuConnection.user || "demo",
  password: herokuConnection.password || "demo",
  database: herokuConnection.database || "demo",
  host: herokuConnection.host || "localhost",
  port: Number(herokuConnection.port) || 5432,
  "type": "postgres",
  "synchronize": false,
  "logging": false,
  "entities": [
    "src/core/database/entities/**/*.ts",
    "build/src/core/database/entities/**/*.js"
  ],
  "migrations": [
    "src/core/database/migrations/*.ts"
  ],
  "cli": {
    "entitiesDir": "src/core/database/entities/",
    "migrationsDir": "src/core/database/migrations/"
  }
};
