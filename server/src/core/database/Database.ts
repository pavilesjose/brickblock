import dotenv from "dotenv";
dotenv.config();
import path from "path";
import { createConnection, getConnection } from "typeorm";
import PgString from "pg-connection-string";

export default class Database {
  static async connect(): Promise<void> {
    const herokuConnection = PgString.parse(process.env.DATABASE_URL);

    await createConnection({
      type: "postgres",
      username: herokuConnection.user || "demo",
      password: herokuConnection.password || "demo",
      database: herokuConnection.database || "demo",
      host: herokuConnection.host || "localhost",
      port: Number(herokuConnection.port) || 5432,
      "entities": [
        "build/src/core/database/entities/**/*.js"
      ],
      "migrations": [
        "build/src/core/database/migrations/**/*.js"
      ],
      
    });
    console.log(`Connected to database ${herokuConnection.database}`);
  }

  static disconnect() {
    getConnection().close();
  }
}
