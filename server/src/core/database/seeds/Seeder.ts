import UserSeeds from "./entities/UserSeeds";
import Database from "../Database";
import ISeed from "./interfaces/ISeed";

const seeds: ISeed[] = [
  new UserSeeds()
]

Database.connect().then(async () => {
  for (const seed of seeds) {
    await seed.seed();
  }

  Database.disconnect();
})
