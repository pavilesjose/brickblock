import ISeed from "../interfaces/ISeed";
import { getRepository, getCustomRepository, getConnection } from "typeorm";
import User from "../../entities/User";
import UserRepository from "../../../../contexts/user/repositories/UserRepository";

export default class UserSeeds implements ISeed {
  async seed() {
    try {
      console.log("Seeding users...")
      const repository = getConnection().getCustomRepository(UserRepository);
    
      for(let i = 0; i < 5; i++){
        const user = new User();
        user.username = `seed${i + 1}`
        user.createdAt = new Date();
        await repository.save(user);
      }

    } catch(e) {
      console.error(e);
    }
  }
}
