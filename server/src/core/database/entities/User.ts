import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity("users")
export default class User {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({
    name: "username",
    type: "character varying",
    unique: true,
    nullable: false})
  username: string;
  @Column({
    name: "created_at",
    type: "timestamp with time zone",
    nullable: false})
  createdAt: Date;
}
