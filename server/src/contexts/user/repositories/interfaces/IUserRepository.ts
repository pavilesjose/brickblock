import { Repository } from "typeorm";
import User from "../../../../core/database/entities/User";

export default interface IUserRepository extends Repository<User> {
  getUserByUsername(username: string): Promise<User>;
}
