import IUserRepository from "./interfaces/IUserRepository";
import { EntityRepository, Repository } from "typeorm";
import User from "../../../core/database/entities/User";

@EntityRepository(User)
export default class UserRepository extends Repository<User> implements IUserRepository {
  getUserByUsername(username: string): Promise<User> {
    return this.findOne({
      where: {
        username,
      }
    })
  }
}
