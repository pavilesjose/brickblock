import UserController from "./controllers/UserController";
import UserRouter from "./router/UserRouter";
import IUserRepository from "./repositories/interfaces/IUserRepository";
import { getConnection } from "typeorm";
import User from "../../core/database/entities/User";
import UserRepository from "./repositories/UserRepository";

export default class UserModule {
  static getRouter(endpoint: string) {
    return new UserRouter(endpoint, this.getController());
  }

  static getController() {
    return new UserController();
  }

  static getRepository(): IUserRepository {
    return getConnection().getCustomRepository(UserRepository);
  }
}
