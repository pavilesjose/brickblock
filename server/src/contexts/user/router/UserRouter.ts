import UserController from "../controllers/UserController";
import express, {Request, Response} from "express";

export default class UserRouter {
  
  private rootRouter: express.Router;
  private router: express.Router;

  constructor(private rootEndpoint: string, private sampleController: UserController){
    this.rootRouter = express.Router();
    this.router = express.Router();
    this.rootRouter.use(this.rootEndpoint, this.router);
  }

  getRoutes() {

    this.router.get("/", this.index());
    return this.rootRouter;
  }

  index() {
    return(req: Request, res: Response) => {
      this.sampleController.index()
      .then(result => res.json(result))
    }
  }
}
