import dotenv from "dotenv";
dotenv.config();
import express from "express";
import bodyParser from "body-parser";
import Application from "./Application";
const port = process.env.PORT || 3000;
const expressApp = express();
const expressRouter = express.Router();

expressApp.use(bodyParser.json());

const application = new Application(expressApp, expressRouter)
application.init();

expressApp.listen(port, () => console.log(`Server is online at port ${port}`))
