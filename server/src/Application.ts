import express from "express";
import TurnModule from "./contexts/turns/TurnModule";
import UserModule from "./contexts/user/UserModule";
import Database from "./core/database/Database";

export default class Application {
  constructor(private app: express.Application, private router: express.Router){
    this.app.use("/api/v1", this.router);
  }
  async init() {
    await Database.connect();
    this.router.use(UserModule.getRouter("/user").getRoutes());

  }
}
